# Virtual Joystick

## Table of content

1. Getting started
2. Case of use (Spaceship)
3. Customizing

## Getting started

![Image 1](img/01.jpg)
Drag the object `vjController` in to your room

### Configuring your player object

You need at least two events in your player object
- create
- draw

![Image 2](img/02.jpg)

#### Create (event)
Get the `DeviceController` instance defined in `vJoystick_init`, then get the Left and Right joystick instances

![Image 3](img/03.jpg)

#### Draw (event)

Get the direction and / or rotation data from the joysticks to controll your player object

![Image 4](img/04.jpg)

---

## Case of use

You want to control a spaceship, this space ship can fly free in the screen (up, down, left and right).

The spaceship can rotate without affect their direction to shoot enemies

 > The spaceship and shoot sprite is not included in the package

### The player object (spaceship)

It has tre events:
- Create: To initialize all
- Draw: To draw the ship and control movment
- Alarm to control the shoot delay

![Image 6](img/06.jpg)

**Create event**

![Image 7](img/07.jpg)

The `shooting` variable determines when the spaceship is shooting

You can modify the shoot delay in the variable `shoot_delay`

The function `shoot(_direction)` creates a shoot object in the direction passed as argument.

**Draw event**

![Image 8](img/08.jpg)

The spaceship shoot only when the right joystick is active and in the right joystick direction.

**Alarm 0**

![Image 9](img/09.jpg)

If the right joystick is active, shoot and prepares the next alarm

## Customizing

You can change the initial configuration we provided in `vJoystick_init`, here you can change:

- x and y position of each joystick
- The main radius (mainRadius)
- The inner joystick radius (jr)
- The dead-zone radius (dzr)

![Image 10](img/10.jpg)

You can change all of these values programaticaly with the functions provided in `vjVirtualJoystick`, you can use the getter and setter functions to manipulate the joystick parameters.

More customizations are coming soon !

